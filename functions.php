<?php
/**
 * ctb functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ctb
 */

if (!function_exists('ctb_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function ctb_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on ctb, use a find and replace
         * to change 'ctb' to the name of your theme in all the template files.
         */
        load_theme_textdomain('ctb', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-1' => esc_html__('Primary', 'ctb'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('ctb_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }
endif;
add_action('after_setup_theme', 'ctb_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ctb_content_width()
{
    $GLOBALS['content_width'] = apply_filters('ctb_content_width', 640);
}

add_action('after_setup_theme', 'ctb_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ctb_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'ctb'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'ctb'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'ctb_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function ctb_styles()
{


    wp_register_style('bootstrap', get_template_directory_uri() . '/dist/css/bootstrap.css');
    wp_register_style('bootstrap-theme', get_template_directory_uri() . '/dist/css/bootstrap-theme.css');
    wp_register_style('demo', get_template_directory_uri() . '/dist/css/demo.css');
    wp_register_style('normalize', get_template_directory_uri() . '/dist/css/normalize.css');
    wp_register_style('style3', get_template_directory_uri() . '/dist/css/style3.css');
    wp_register_style('slick', get_template_directory_uri() . '/dist/css/slick.css');
    wp_register_style('slick-theme', get_template_directory_uri() . '/dist/css/slick-theme.css');
    wp_register_style('jquery.fancybox', get_template_directory_uri() . '/dist/css/jquery.fancybox.css');
    wp_register_style('app', get_template_directory_uri() . '/dist/css/app.css');


    wp_enqueue_style('bootstrap');
    wp_enqueue_style('bootstrap-theme');
    wp_enqueue_style('demo');
    wp_enqueue_style('normalize');
    wp_enqueue_style('style3');
    wp_enqueue_style('slick');
    wp_enqueue_style('slick-theme');
    wp_enqueue_style('jquery.fancybox');
    wp_enqueue_style('app');
}

add_action('wp_enqueue_scripts', 'ctb_styles');

function ctb_scripts()
{
    wp_enqueue_style('ctb-style', get_stylesheet_uri());

    wp_enqueue_script('ctb-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true);

    wp_enqueue_script('ctb-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);

    wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js', array(), 3.1);
    wp_register_script('bootstrap-js', get_template_directory_uri() . '/dist/js/bootstrap.js');
    wp_register_script('classie', get_template_directory_uri() . '/dist/js/classie.js');
    wp_register_script('demo1', get_template_directory_uri() . '/dist/js/demo1.js');
    wp_register_script('jquery.parallax', get_template_directory_uri() . '/js/jquery.parallax.js');
    wp_register_script('jquery.fancybox', get_template_directory_uri() . '/dist/js/jquery.fancybox.js');
    wp_register_script('slick', get_template_directory_uri() . '/dist/js/slick.js');
    wp_register_script('ini-slide', get_template_directory_uri() . '/dist/js/ini-slide.js');
    wp_register_script('target-script', get_template_directory_uri() . '/dist/js/target-script.js');
    wp_register_script('modernizr', get_template_directory_uri() . '/dist/js/modernizr.custom.js');
    wp_register_script('app-js', get_template_directory_uri() . '/dist/js/app.js');


    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap-js');
    wp_enqueue_script('classie');
    wp_enqueue_script('jquery.fancybox');
    wp_enqueue_script('slick');
    wp_enqueue_script('ini-slide');
    wp_enqueue_script('target-script');
    wp_enqueue_script('jquery.parallax');
    wp_enqueue_script('demo1');
    wp_enqueue_script('modernizr');
    wp_enqueue_script('app-js');

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'ctb_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}
add_filter('the_content', 'replace_url_to_https', 30);
function replace_url_to_https($text)
{
    $text = preg_replace('~http(://(?:www.)?' . preg_quote($_SERVER['HTTP_HOST']) . ')~', 'https\1', $text);
    return $text;
}

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');