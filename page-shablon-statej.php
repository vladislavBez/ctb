<?php
/*
Template Name: Страница с контентом
*/
include 'header-page.php';
?>
<body>
<div class="modal fade" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel">Подобрать тур</h4></center>
            </div>
            <div class="modal-body">
                <form action="https://ctb76.ru/podbor.php" class="contact" method="post" name="test" id="form">
                    <label for="name">ФИО:</label>
                    <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name" class="form-control" required="required"></p>
                    <label for="phone">Телефон:</label>
                    <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone" class="form-control" required="required"></p>
                    <label for="email">E-mail:</label>
                    <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail" class="form-control" required="required"></p>
                    <label for="message">Сообщение:</label><br/>
                    <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
                    <label for="name" style="color: black;">Введите цифры с картинки:</label>
                    <!-- вывод капчи из файла captcha.php -->
                    <p><?php require ("captcha.php"); ?></p>
                    <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                    <center><input type="submit" class="btn btn-lg red"  id="submit" name="form" value="Отправить"></center>
                </form>
            </div>
        </div>
    </div>
</div>
<!--первый баннер -->
<div class="container-fluid padd-bott">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <div class="post-title">
                <div class="img-responsive">
                    <?php the_post_thumbnail(); ?>
                </div>
                <div class="post-title-block hidden-xs ">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!--конец первого баннера-->
<div class="container  ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visible-xs padd-bot padd-top-3">
        <h2 class="gold post-title-xs">
            <?php the_title(); ?>
        </h2>
    </div>
    <div class="row padd-bot">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php the_content(); ?>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="post-slider">
                <div>
                    <a data-fancybox="gallery"
                       href="https://ctb76.ru/wp-content/uploads/2018/07/IMG_20170611_112729-1.jpg">
                        <img class="img-responsive post-img"
                             src="https://ctb76.ru/wp-content/uploads/2018/07/IMG_20170611_112729-1.jpg">
                    </a>
                </div>
                <div>
                    <a data-fancybox="gallery"
                       href="https://ctb76.ru/wp-content/uploads/2018/07/IMG_20170611_151326-1.jpg">
                        <img class="img-responsive post-img"
                             src="https://ctb76.ru/wp-content/uploads/2018/07/IMG_20170611_151326-1.jpg">
                    </a>
                </div>

                <div>
                    <a data-fancybox="gallery"
                       href="https://ctb76.ru/wp-content/uploads/2018/07/IMG_20170615_211514-1.jpg">
                        <img class="img-responsive post-img"
                             src="https://ctb76.ru/wp-content/uploads/2018/07/IMG_20170615_211514-1.jpg">
                    </a>
                </div>
                <div>
                    <a data-fancybox="gallery"
                       href="https://ctb76.ru/wp-content/uploads/2018/07/IMG_20170620_164556_542-1.jpg">
                        <img class="img-responsive post-img"
                             src="https://ctb76.ru/wp-content/uploads/2018/07/IMG_20170620_164556_542-1.jpg">
                    </a>
                </div>
                <div>
                    <a data-fancybox="gallery"
                       href="https://ctb76.ru/wp-content/uploads/2018/07/IMG_20170620_222327_991-1.jpg">
                        <img class="img-responsive post-img"
                             src="https://ctb76.ru/wp-content/uploads/2018/07/IMG_20170620_222327_991-1.jpg">
                    </a>
                </div>
                <div>
                    <a data-fancybox="gallery"
                       href="https://ctb76.ru/wp-content/uploads/2018/07/IMG-20170617-WA0055-1.jpeg">
                        <img class="img-responsive post-img "
                             src="https://ctb76.ru/wp-content/uploads/2018/07/IMG-20170617-WA0055-1.jpeg">
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>
<?php wp_footer(); ?>
<?php get_footer('page'); ?>