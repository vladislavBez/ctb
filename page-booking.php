<?php
/*
Template Name: Страница Раннее Бронирование
*/
include 'header-page.php';
?>
<!-- Modal -->
<div class="modal fade" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Подобрать тур</h4></center>
                </div>
                <div class="modal-body">
                    <form action="https://ctb76.ru/podbor.php" class="contact" method="post" name="test" id="form">
                        <label for="name">ФИО:</label>
                        <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name" class="form-control" required="required"></p>
                        <label for="phone">Телефон:</label>
                        <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone" class="form-control" required="required"></p>
                        <label for="email">E-mail:</label>
                        <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail" class="form-control" required="required"></p>
                        <label for="message">Сообщение:</label><br/>
                        <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
						<label for="name" style="color: black;">Введите цифры с картинки:</label>
                        <!-- вывод капчи из файла captcha.php -->
						<p><?php require ("captcha.php"); ?></p>
                        <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                        <center><input type="submit" class="btn btn-lg red"  id="submit" name="form" value="Отправить"></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="myModal-8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Подбор раннего бронирования</h4></center>
                </div>
                <div class="modal-body">
                    <? echo do_shortcode('[contact-form-7 id="472" title="Форма раннего бронирования"]');?>
                </div>
            </div>
        </div>
    </div>
<!---->
<body>
<div class="container-fluid bron-back-banner">

    <div class="container">
        <div class="col-lg-12"><h1 class="white event-h1"><?php the_title(); ?></h1></div>
        <div class="col-lg-12 bonus-banner-lvl2"><strong class="gold text-size-4">ПЛАТИ РАНЬШЕ, НО МЕНЬШЕ</strong></div>
        <div class="col-lg-12 cash-padd-4">
            <button data-toggle="modal" data-target="#myModal-8" type="button" class="btn btn-lg red">ПОЛУЧИТЬ ПОДБОРКУ ТУРОВ</button>
        </div>
    </div>
</div>
    <div class="container-fluid mulmak-bone hot-padd">
        <div class="container">
            <strong class="text-size-4">15 ПОВОДОВ КУПИТЬ ТУР</strong>
            <div>
                <p>15 реальных поводов купить тур по раннему бронированию.<br>
                Отпуск – чудесная пора! Мы его ждём, о нём мечтаем, его планируем, придумываем место, время, наполнение. Даже от самой любимой работы на свете всем хочется отдохнуть. Наша рекомендация – подумать об отпуске заранее и приобрести путёвку по акциям Раннего Бронирования (РБ). ПОЧЕМУ?<br>
                <strong>Повод №1: Экономия бюджета на отдых до 50%.</strong><br>
                Действительно, по раннему бронированию отели дают очень приличные скидки. Им выгодно продать заранее, поэтому контрактные</p>
                <a href="//ctb76.ru/488-2/" class="btn btn-lg red">
                    Читать далее...
                </a>
            </div>
        </div>
    </div>
<div class="container-fluid hot-padd">
    <div class="container">
        <strong class="text-size-4">ЧТО ТАКОЕ РАННЕЕ БРОНИРОВАНИЕ (РБ)?</strong>
        <p>Акции РБ - это сезонные предложения туроператоров, когда те же самые туры ЗАРАНЕЕ стоят ДЕШЕВЛЕ, чем перед
            вылетом</p>
    </div>
</div>
<div class="container-fluid mulmak-bone hot-padd">
    <div class="container">
        <strong class="text-size-4">ПОЧЕМУ НАДО ПОКУПАТЬ?</strong>
        <p>
        - если вы хотите сэкономить <br>
        - если вы хотите иметь широкий выбор предложений (отелей, авиакомпаний, рейсов) <br>
        - если вы хотите гарантированно отдохнуть, особенно в высокий сезон и на популярном курорте <br>
        - если ваш отпуск привязан к четким датам</p>

    </div>
</div>
<div class="container-fluid hot-padd">
    <div class="container">
        <strong class="text-size-4">КОГДА ПОКУПАТЬ?</strong>
        <p>За 3-6 месяцев до начала поездки</p>
    </div>
</div>
<div class="container-fluid mulmak-bone hot-padd">
    <div class="container">
        <strong class="text-size-4 ">ЧТО ПОКУПАТЬ?</strong>
        <p>Летом - на зиму: <br>
            экзотика: Тайланд, Индия, Вьетнам, ОАЭ, Куба, Доминикана, Мексика, новый год, горные лыжи, школьные каникулы
            и др.<br>
            Зимой - на лето: <br>
            майские праздники, Турция, Греция, Кипр, Болгария, Крым, Краснодарский край и др.</p>
    </div>
</div>
<div class="container-fluid hot-padd">
    <div class="container">
        <strong class="text-size-4">КАКИЕ УСЛОВИЯ?</strong>
        <p>Каждый туроператор выставляет свои требования по оплате путевки. Как правило, первый платеж составляет 1/3 от
            стоимости тура; до окончания действия акции необходимо оплатить 1/2 часть, срок полной оплаты может быть за
            2 недели до вылета.</p>
    </div>
</div>
	<?php wp_footer(); ?>
<?php get_footer('page'); ?>