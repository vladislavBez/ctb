<?php
/*
Template Name: Страница Поиск Тура
*/
include 'header-page.php';
?>
<!-- Modal -->
<div class="modal fade" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Подобрать тур</h4></center>
                </div>
                <div class="modal-body">
                    <form action="https://ctb76.ru/podbor.php" class="contact" method="post" name="test" id="form">
                        <label for="name">ФИО:</label>
                        <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name" class="form-control" required="required"></p>
                        <label for="phone">Телефон:</label>
                        <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone" class="form-control" required="required"></p>
                        <label for="email">E-mail:</label>
                        <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail" class="form-control" required="required"></p>
                        <label for="message">Сообщение:</label><br/>
                        <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
						<label for="name" style="color: black;">Введите цифры с картинки:</label>
                        <!-- вывод капчи из файла captcha.php -->
						<p><?php require ("captcha.php"); ?></p>
                        <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                        <center><input type="submit" class="btn btn-lg red"  id="submit" name="form" value="Отправить"></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
<!--Modal-->
<body>
<div class="container-fluid search-banner-back">
    <div class="row">
        <div class="container">
            <h1 class="white event-h1">ПОИСК ТУРА</h1>
			<p class="second-title white">Выбор идеального тура для путешествия - это большая работа, в которой много важных нюансов: надежный туроператор, проверенная авиакомпания, удобное время вылетов, отвечающий всем требованиям отель, выбор оптимальных условий страхования и многое многое другое. 
Это наша работа и мы делаем ее качественно и профессионально!</p>
        </div>
    </div>
</div>
<div class="container-fluid back-black ">
    <div class="row text-center partners-logo">
        <h2 class="gold text-size-6 padd-top">ВЫБЕРИ СВОЙ ВАРИАНТ</h2>
    </div>
    <div class="container personal-cart">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  center-block">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 search-chs-1">

                <div class="choice-div">
                    <strong class="text-size-4 white choice-strong">01</strong>
                </div>
                <div class="choice-div">
                    <p class="text-size-3 white">Направление знаю, <br>
                        хочу сам выбрать <br>
                        конкретный тур с ценой
                    </p>
                </div>
                <div class="choice-div choice-button-4">
					<form action="https://ctb76.ru/podbor-tura/" target="_blank">
                    <button type="submit" class="btn btn-lg red">ВЫБРАТЬ ТУР</button>
					</form>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 search-chs-2"><div class="choice-div">

                <strong class="text-size-4 white choice-strong">02</strong>
            </div>
                <div class="choice-div">
                    <p class="text-size-3 white">Не хочу тратить <br>
                        время, хочу <br>
                        доверить отдых <br>
                        профессионалам
                    </p>
                </div>
                <div class="choice-div choice-button-3">
                    <button type="button" class="btn btn-lg red" data-toggle="modal" data-target="#myModal-2">ПОДОБРАТЬ ТУР</button>
                </div>
            </div>
        </div>
    </div>
</div>
	<?php wp_footer(); ?>
<?php get_footer('page'); ?>