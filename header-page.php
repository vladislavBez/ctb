<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="favicon.png" type="image/png">
    <?php wp_head(); ?>
    <meta name="keywords"
          content="Центр Туристического Бронирования ярославль, ЦТБ, горящие путевки, ЦТБ поиск туров, горящие путевки, горящие путевки Ярославль, горящие туры, подбор тура, Раннее Бронирование, сайт поиска тура">
</head>
<header>
    <div class="header-page" id="menu">

        <div class="container-fluid">
            <div class="col-lg-3 col-md-3 col-sm-2 col-xs-3 text-right logo-z-index">
                <a href="<? echo get_home_url() ?>"><img class="logo"
                                                         src="<?php bloginfo("template_directory"); ?>/img/logo.png"
                                                         alt="ЦТБ" title="Главная"></a>
            </div>
            <div class="nav">
                <div class="col-lg-6 col-md-6 col-sm-7 col-xs-6 text-center smal-alight-social">
                    <div class="btn-group btn-group-justified">
                        <a class="nav-button" role="button" href="https://vk.com/ctb76" target="_blank"
                           title="Мы Вконтакте">
                            <img class=""
                                 src="<?php bloginfo("template_directory"); ?>/img/svg/vk.svg"
                                 alt="Мы Вконтакте">
                        </a>
                        <a class="nav-button" href="https://www.instagram.com/ctb76.ru/" target="_blank"
                           title="Мы в Instagram">
                            <img class=""
                                 src="<?php bloginfo("template_directory"); ?>/img/svg/instagram.svg"
                                 alt="Мы в Instagram">
                        </a>
                        <a class="nav-button" href="https://www.facebook.com/groups/584779218582089/" target="_blank"
                           title="Мы в Facebook">
                            <img class=""
                                 src="<?php bloginfo("template_directory"); ?>/img/svg/facebook.svg"
                                 alt="Мы в Facebook">
                        </a>
                        <a class="nav-button" href="https://ok.ru/group/55405382270984" target="_blank"
                           title="Мы в Одноклассники">
                            <img class=""
                                 src="<?php bloginfo("template_directory"); ?>/img/svg/odnoklassniki-logo (1).svg"
                                 alt="Мы в Одноклассники">
                        </a>
                        <a class="nav-button phone-small-none" role="button" href="tel:+7-4852-94-14-50"
                           title="Телефон ЦТБ">94-14-50</a>
                    </div>
                    <!-- Кнопка, вызывающее модальное окно -->
                    <button data-toggle="modal" data-target="#myModal-2" type="button" class="btn btn-lg red">Подобрать
                        тур
                    </button>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 burger-lk text-center">
                    <section>
                        <a href="https://ctb76.u-on.ru" target="_blank"><img class="lk lk-bck"
                                                                             src="<?php bloginfo("template_directory"); ?>/img/Lk-empty.png"
                                                                             alt="ЛИЧНЫЙ КАБИНЕТ ТУРИСТА"
                                                                             data-toggle="tooltip"
                                                                             title="ЛИЧНЫЙ КАБИНЕТ ТУРИСТА"></a>
                        <button id="trigger-overlay" type="button">
                            <span class="menu-word">Меню</span>
                            <img class="burger-e" src="<?php bloginfo("template_directory"); ?>/img/burger-old.png"
                                 alt="Меню"></button>
                    </section>
                </div>
            </div>
        </div>
        <!-- open/close -->
        <div class="overlay overlay-slidedown">
            <button type="button" class="overlay-close">Close</button>
            <a type="button" href="<? echo get_home_url() ?>" class="overlay-home">Home</a>
            <nav><?php wp_nav_menu($args); ?></nav>
        </div>
    </div>
    <script>
        window.onscroll = function () {
            var scrolled = window.pageYOffset || document.documentElement.scrollTop;
            document.getElementById('menu').style.backgroundColor = scrolled == 0 ? "#ffffff00" : "black";
        }
    </script>

</header>
<img data-toggle="modal" class="subscription-button hidden-xs" data-target="#subscription"
     src="<? echo get_template_directory_uri() . '/dist/img/envelope.svg' ?>" alt="">
<div class="modal fade" id="subscription" tabindex="-1" role="dialog" aria-labelledby="subscription-form"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Подписка на рассылку спецпредложений</h4>
            </div>
            <div class="modal-body">

                <div id="mc_embed_signup input-group">
                    <form action="https://ctb76.us17.list-manage.com/subscribe/post?u=2069bf415a529faa07cfc03d6&amp;id=7a3b003c20"
                          method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form"
                          class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll" class="">
                            <div class="indicates-required padd-bott"><span class="asterisk">*</span>Обязательные поля
                            </div>
                            <div class="mc-field-group padd-bott-1">
                                <label for="mce-EMAIL">Email адрес <span class="asterisk">*</span>
                                </label>
                                <input type="email" value="" name="EMAIL" class="required email form-control"
                                       id="mce-EMAIL">
                            </div>
                            <div class="mc-field-group">
                                <label for="mce-FNAME">Имя </label>
                                <input type="text" value="" name="FNAME" class="form-control" id="mce-FNAME">
                            </div>
                            <div class="modal-footer">
                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response" style="display:none"></div>
                                    <div class="response" id="mce-success-response" style="display:none"></div>
                                </div>
                                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text"
                                                                                                          name="b_2069bf415a529faa07cfc03d6_7a3b003c20"
                                                                                                          tabindex="-1"
                                                                                                          value="">
                                </div>
                                <div class="clear"><input type="submit" value="Подписаться" name="subscribe"
                                                          id="mc-embedded-subscribe" class="button btn btn-lg red">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <script type='text/javascript'
                        src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
                <script type='text/javascript'>(function ($) {
                        window.fnames = new Array();
                        window.ftypes = new Array();
                        fnames[0] = 'EMAIL';
                        ftypes[0] = 'email';
                        fnames[1] = 'FNAME';
                        ftypes[1] = 'text';
                        fnames[2] = 'LNAME';
                        ftypes[2] = 'text';
                        fnames[3] = 'ADDRESS';
                        ftypes[3] = 'address';
                        fnames[4] = 'PHONE';
                        ftypes[4] = 'phone';
                        fnames[5] = 'BIRTHDAY';
                        ftypes[5] = 'birthday';
                        /*
                        * Translated default messages for the $ validation plugin.
                        * Locale: RU
                        */
                        $.extend($.validator.messages, {
                            required: "Это поле необходимо заполнить.",
                            remote: "Пожалуйста, введите правильное значение.",
                            email: "Пожалуйста, введите корректный адрес электронной почты.",
                            url: "Пожалуйста, введите корректный URL.",
                            date: "Пожалуйста, введите корректную дату.",
                            dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
                            number: "Пожалуйста, введите число.",
                            digits: "Пожалуйста, вводите только цифры.",
                            creditcard: "Пожалуйста, введите правильный номер кредитной карты.",
                            equalTo: "Пожалуйста, введите такое же значение ещё раз.",
                            accept: "Пожалуйста, выберите файл с правильным расширением.",
                            maxlength: $.validator.format("Пожалуйста, введите не больше {0} символов."),
                            minlength: $.validator.format("Пожалуйста, введите не меньше {0} символов."),
                            rangelength: $.validator.format("Пожалуйста, введите значение длиной от {0} до {1} символов."),
                            range: $.validator.format("Пожалуйста, введите число от {0} до {1}."),
                            max: $.validator.format("Пожалуйста, введите число, меньшее или равное {0}."),
                            min: $.validator.format("Пожалуйста, введите число, большее или равное {0}.")
                        });
                    }(jQuery));
                    var $mcj = jQuery.noConflict(true);</script>
                <!--End mc_embed_signup-->
            </div>


        </div>
    </div>

    <!-- Begin MailChimp Signup Form -->

</div>
