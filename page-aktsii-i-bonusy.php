<?php
/*
Template Name: Страница Акции и Бонусы
*/
include 'header-page.php';
?>
<!-- Modal -->
<div class="modal fade" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Подобрать тур</h4></center>
                </div>
                <div class="modal-body">
                    <form action="https://ctb76.ru/podbor.php" class="contact" method="post" name="test" id="form">
                        <label for="name">ФИО:</label>
                        <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name" class="form-control" required="required"></p>
                        <label for="phone">Телефон:</label>
                        <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone" class="form-control" required="required"></p>
                        <label for="email">E-mail:</label>
                        <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail" class="form-control" required="required"></p>
                        <label for="message">Сообщение:</label><br/>
                        <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
						<label for="name" style="color: black;">Введите цифры с картинки:</label>
                        <!-- вывод капчи из файла captcha.php -->
						<p><?php require ("captcha.php"); ?></p>
                        <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                        <center><input type="submit" class="btn btn-lg red"  id="submit" name="form" value="Отправить"></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="myModal-5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">ПОЛУЧИТЬ РАССЫЛКУ</h4></center>
                </div>
                <div class="modal-body">
                    <form action="http://ctb76.ru/distribution.php" class="contact" method="post" name="test" id="form">
                        <label for="name">ФИО:</label>
                        <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name" class="form-control" required="required"></p>
                        <label for="phone">Телефон:</label>
                        <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone" class="form-control" required="required"></p>
                        <label for="email">E-mail:</label>
                        <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail" class="form-control" required="required"></p>
                        <label for="message">Сообщение:</label><br/>
                        <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
						<label for="name" style="color: black;">Введите цифры с картинки:</label>
                        <!-- вывод капчи из файла captcha.php -->
						<p><?php require ("captcha.php"); ?></p>
                        <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                        <center><input type="submit" class="btn btn-lg red"  id="submit" name="form" value="Отправить"></center>
                    </form>				
                </div>
            </div>
        </div>
 </div>

	<!---->
<body class="news-border">
<!--первый баннер -->
<div class="container-fluid banner-event">
    <div class="row">
        <div class="container">
            <h1 class="white event-h1">АКЦИИ <br> И БОНУСЫ</h1>
        </div>
    </div>
</div>
<!--конец первого баннера-->
<div class="container-fluid back-black">
<div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 back-white">
			 <div id="primary" class="site-content">
               <div id="content" role="main">
                <?php while ( have_posts() ) : the_post(); ?>
               <div class="entry-content">
                <?php the_content(); ?>
               </div><!-- .entry-content --> 
               <?php comments_template( '', true ); ?>
               <?php endwhile; // end of the loop. ?>
               </div><!-- #content -->
             </div><!-- #primary -->
                <button class="btn btn-lg red" type="button" data-toggle="modal" data-target="#myModal-5">ПОЛУЧИТЬ РАССЫЛКУ О СКИДКАХ И АКЦИЯХ</button>
            </div>
        </div>
</div>
    <div class="text-center partners-logo">
        <h2 class="gold text-size-8">Наши предложения</h2>
    </div>
	    <?php query_posts('cat=7&order=ASC'); ?>
        <?php if(have_posts()) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
<div class="container">
            <div id="post-<?php the_id(); ?>" <?php post_class(); ?>
		       <div class="row">
	              <div class="col-lg-5 col-md-5 col-sm-12">
                    <div class="img-responsive"><?php the_post_thumbnail(); ?></div>
			      </div>
	                <div class="col-lg-7  col-md-7 col-sm-12">	  
                     <div class="simple-red white date-news">
                     <p><?php echo (get_post_meta($post->ID, 'date', true)); ?></p>
                     </div>
			         <h3 class="white text-size-4 media-text-size-4"><?php the_title();  ?></h3>
                     <div class="white text-size-1"><?php the_content(); ?></div>
                    </div>      
                </div>
     	    </div>  
        <?php endwhile; ?>
        <?php else : ?>
        <h2>Записей нет</h2>
        <?php endif; ?>
        <?php wp_reset_query(); ?>	
	<?php wp_footer(); ?>
    <?php get_footer('page'); ?>
