<?php

/**
 * Template Name: Страница Наш блог
 */

include 'header-page.php';

?>

<body>

<div class="container-fluid padd-bott">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <div class="post-title">
                <div class="img-responsive">
                    <?php the_post_thumbnail(); ?>
                </div>
                <div class="post-title-block hidden-xs ">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>

<?php query_posts('cat=8&order=ASC'); ?>

<?php if (have_posts()) : ?>

    <?php while (have_posts()) : the_post(); ?>

        <div class="container">


            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blog-padding">

                <div class="">
                    <a href="<? the_permalink(); ?>" class="blog-title-href">
                        <h1><?php the_title(); ?></h1>
                    </a>
                </div>
                <div style="font-weight: 700;"><?php the_excerpt(); ?></div>

                <br>

                <strong class="responsive-low-text"><?php echo(get_post_meta($post->ID, 'name', true)); ?>
                </strong>
                <a href="<? the_permalink(); ?>" class="btn btn-lg red">
                    Читать далее...
                </a>
            </div>

        </div>

    <?php endwhile; ?>

<?php else : ?>

    <h2>Записей нет</h2>

<?php endif; ?>

<?php wp_reset_query(); ?>

<?php wp_footer(); ?>
<?php get_footer('page'); ?>