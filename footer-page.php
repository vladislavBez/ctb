<?php
/**
 * The template for displaying the footer-page
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ctb
 */
?>
</body>
<footer class="footer_page-content">
    <div class="container">
        <!--Нижняя часть с логотимом и надписями-->
        <div class="row padd-bot">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 big-img">
                    <img class="img-responsive" src="<?php bloginfo("template_directory");?>/img/big.png" alt="">
                </div>
                <div class="">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 ">
                        <ul>
                            <li><a href="<?php echo get_permalink(118); ?>">Фото и Видео</a></li>
                            <li><a href="<?php echo get_permalink(289); ?>">Поиск тура</a></li>
                            <li><a href="<?php echo get_permalink(92); ?>">Горящие</a></li>
                            <li><a href="<?php echo get_permalink(95); ?>">Раннее Бронирование</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <ul>
                            <li><a href="<?php echo get_permalink(97); ?>">Акции и Бонусы</a></li>
                            <li><a href="<?php echo get_permalink(100); ?>">Отзывы</a></li>
                            <li><a href="<?php echo get_permalink(103); ?>">Услуги</a></li>
                            <li><a href="<?php echo get_permalink(105); ?>">Программа привилегий</a></li>
                        </ul>
                    </div>
                    <div class="gold col-lg-7 col-md-12 page-footer-text text-center">
                        <strong class="font-size-2">ООО "ЦЕНТР ТУРИСТИЧЕСКОГО БРОНИРОВАНИЯ"</strong>
                    </div>
                </div>
            </div>
        </div>
        <!--Самый низ-->
    </div>
</footer>
</html>
