<?php
/*
Template Name: Услуги 
*/
include 'header-page.php';
?>
<!-- Modal -->
<div class="modal fade" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Подобрать тур</h4></center>
                </div>
                <div class="modal-body">
                    <form action="https://ctb76.ru/podbor.php" class="contact" method="post" name="test" id="form">
                        <label for="name">ФИО:</label>
                        <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name" class="form-control" required="required"></p>
                        <label for="phone">Телефон:</label>
                        <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone" class="form-control" required="required"></p>
                        <label for="email">E-mail:</label>
                        <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail" class="form-control" required="required"></p>
                        <label for="message">Сообщение:</label><br/>
                        <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
						<label for="name" style="color: black;">Введите цифры с картинки:</label>
                        <!-- вывод капчи из файла captcha.php -->
						<p><?php require ("captcha.php"); ?></p>
                        <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                        <center><input type="submit" class="btn btn-lg red"  id="submit" name="form" value="Отправить"></center>
                    </form>
                </div>
            </div>
        </div>
   </div>
<div class="modal fade" id="myModal-10" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Получить консультацию</h4></center>
                </div>
                <div class="modal-body">
                    <form action="http://ctb76.ru/consultation.php" class="contact" method="post" name="test" id="form">
                        <label for="name">ФИО:</label>
                        <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name" class="form-control" required="required"></p>
                        <label for="phone">Телефон:</label>
                        <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone" class="form-control" required="required"></p>
                        <label for="email">E-mail:</label>
                        <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail" class="form-control" required="required"></p>
                        <label for="message">Сообщение:</label><br/>
                        <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
						<label for="name" style="color: black;">Введите цифры с картинки:</label>
                        <!-- вывод капчи из файла captcha.php -->
						<p><?php require ("captcha.php"); ?></p>
                        <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                        <center><input type="submit" class="btn btn-lg red"  id="submit" name="form" value="Отправить"></center>
                    </form>
                </div>
            </div>
        </div>
   </div>
<!-- Modal -->
<body>
<div class="container-fluid service-banner">

    <div class="container">
        <h1 class="white event-h1">УСЛУГИ</h1>
        <div class="keep col-lg-4 col-md-3 col-sm-4 col-xs-7">
            <img class="img-responsive" src="<?php bloginfo("template_directory");?>/img/soon.png" alt="">
        </div>
    </div>
</div>

<div class="container service-nav ">
	<!-- Nav tabs -->
    <ul class="tabs nagate-service-lg">
        <li class="active"><a href="#home" data-toggle="tab" class="white"><strong>ТУРЫ ЗА ГРАНИЦУ</strong></a></li>
        <li><a href="#a" data-toggle="tab" class="white"><strong>ТУРЫ ПО РОССИИ</strong></a></li>
        <li><a href="#b" data-toggle="tab" class="white"><strong>АВИАБИЛЕТЫ</strong></a></li>>
        <li><a href="#c" data-toggle="tab" class="white"><strong>ТРАНСФЕРЫ</strong></a></li>
        <li><a href="#d" data-toggle="tab" class="white"><strong>ОТЕЛИ</strong></a></li>
        <li><a href="#e" data-toggle="tab" class="white"><strong>СТРАХОВАНИЕ</strong></a></li>
        <li><a href="#v" data-toggle="tab" class="white"><strong>КРУИЗЫ</strong></a></li>
        <li><a href="#w" data-toggle="tab" class="white"><strong>ВИЗЫ</strong></a></li>
        <li><a href="#z" data-toggle="tab" class="white"><strong>ПРОЧЕЕ</strong></a></li>
    </ul>
    <div class="btn-group navigate-service-sm">
        <button type="button" class="btn btn-lg red dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Услуги<span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><a href="#a">ТУРЫ ПО РОССИИ</a></li>
            <li><a href="#b">АВИАБИЛЕТЫ</a></li>
            <li><a href="#c">ТРАНСФЕРЫ</a></li>
            <li><a href="#d">ОТЕЛИ</a></li>
            <li><a href="#e">СТРАХОВАНИЕ</a></li>
            <li><a href="#v">КРУИЗЫ</a></li>
            <li><a href="#w">ВИЗЫ</a></li>
            <li><a href="#z">ПРОЧЕЕ</a></li>
        </ul>
    </div>
</div>
<div class="tab-content">
  <div class="tab-pane active" id="home">
	  <div class="container service-content">
    <ul>
        <li>Пакетные</li>
        <li>Экскурсионные</li>
        <li>Индивидульные</li>
    </ul>
    <div class="service-but">
        <button data-toggle="modal" data-target="#myModal-10" type="button" class="btn btn-lg red">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
    </div>
</div>
	</div>
  <div class="tab-pane" id="a">
	  <div class="container service-content">
    <ul>
		<li>Эксркурсионные</li>
        <li>Однодневные</li>
        <li>Ж/д, автобус на Юг</li>
        <li>Речные круизы</li>
    </ul>
    <div class="service-but">
       <button data-toggle="modal" data-target="#myModal-10" type="button" class="btn btn-lg red">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
    </div>
</div>
	
	</div>
  <div class="tab-pane" id="b">
	  <div class="container service-content">
    <ul>
        <li>Чартерные</li>
        <li>Регулярные</li>
        
    </ul>
    <div class="service-but">
       <button data-toggle="modal" data-target="#myModal-10" type="button" class="btn btn-lg red">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
    </div>
</div>
	
	</div>
	  <div class="tab-pane" id="c">
	  <div class="container service-content">
    <ul>
        <li>Групповые</li>
        <li>Индивидуальные</li>
    </ul>
    <div class="service-but">
        <button data-toggle="modal" data-target="#myModal-10" type="button" class="btn btn-lg red">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
    </div>
</div>
	
	</div>
	  <div class="tab-pane" id="d">
	  <div class="container service-content">
    <ul>
        <li>Россия</li>
        <li>Заграница</li>
    </ul>
    <div class="service-but">
        <button data-toggle="modal" data-target="#myModal-10" type="button" class="btn btn-lg red">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
    </div>
</div>
	
	</div>
	  <div class="tab-pane" id="e">
	  <div class="container service-content">
    <ul>
        <li>Медицина</li>
        <li>От невыезда</li>
		<li>Любые риски</li>
    </ul>
    <div class="service-but">
        <button data-toggle="modal" data-target="#myModal-10" type="button" class="btn btn-lg red">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
    </div>
</div>
	
	</div>
	  <div class="tab-pane" id="v">
	  <div class="container service-content">
    <ul>
        <li>Морские</li>
        <li>Речные</li>
        <li>Межконтинентальные</li>
    </ul>
    <div class="service-but">
        <button data-toggle="modal" data-target="#myModal-10" type="button" class="btn btn-lg red">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
    </div>
</div>
	
	</div>
	  <div class="tab-pane" id="w">
	  <div class="container service-content">
    <ul>
        <li>С оформлением в Москве</li>
        <li>Ярославле</li>
        <li>Вологде</li>
    </ul>
    <div class="service-but">
        <button data-toggle="modal" data-target="#myModal-10" type="button" class="btn btn-lg red">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
</div>
		</div>
	</div>
	  <div class="tab-pane" id="z">
	  <div class="container service-content">	
		   <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 ">
                      <ul>
                           <li>Лечение и профилактика за границей</li> 
                           <li>Праздничный и событийный туризм</li> 
                           <li>Для любителей экстрима и активного отдыха</li> 
                           <li>Туры для организованных групп</li>   
			          </ul>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <ul>
                            <li>MICE (деловой туризм)</li>
                            <li>Шоп-туры</li>
                            <li>Образование за рубежом</li>
                            <li>Любые нестандартные запросы</li>
                        </ul>
                    </div>
		           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		  	      <div class="service-but">
                  <button data-toggle="modal" data-target="#myModal-10" type="button" class="btn btn-lg red">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
            </div>
          </div>
	  </div>
	</div>
</div>	
<?php wp_footer(); ?>
<?php get_footer('page'); ?>