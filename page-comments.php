<?php
/*
Template Name: Страница с отзывами
*/
include 'header-page.php';
?>
<!-- Modal -->
<div class="modal fade" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Подобрать тур</h4></center>
                </div>
                <div class="modal-body">
                    <form action="https://ctb76.ru/podbor.php" class="contact" method="post" name="test" id="form">
                        <label for="name">ФИО:</label>
                        <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name" class="form-control" required="required"></p>
                        <label for="phone">Телефон:</label>
                        <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone" class="form-control" required="required"></p>
                        <label for="email">E-mail:</label>
                        <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail" class="form-control" required="required"></p>
                        <label for="message">Сообщение:</label><br/>
                        <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
						<label for="name" style="color: black;">Введите цифры с картинки:</label>
                        <!-- вывод капчи из файла captcha.php -->
						<p><?php require ("captcha.php"); ?></p>
                        <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                        <center><input type="submit" class="btn btn-lg red"  id="submit" name="form" value="Отправить"></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="myModal-4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <center><h4 class="modal-title" id="myModalLabel">Оставить отзыв</h4></center>
                    </div>
                    <div class="modal-body">
                        <? echo do_shortcode('[contact-form-7 id="458" title="Без названия"]');?>
                    </div>
                </div>
            </div>
        </div>
<!--Modal-->
<body>
<div class="container-fluid response-back">
    <div class="container">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 responsive-banner-text">
            <h1 class="white event-h1">ОТЗЫВЫ</h1>
            <p class="second-title white">Мнение каждого туриста очень важно и ценно для нас
                Здесь вы найдете отзывы наших постоянных туристов и сможете поделиться
                своими впечатлениями об отдыхе и работе наших специалистов</p>
            <button type="button" class="btn btn-lg red" data-toggle="modal" data-target="#myModal-4">ОСТАВИТЬ ОТЗЫВ</button>
        </div>
    </div>
</div>
	<?php query_posts('cat=4&order=ASC'); ?>
        <?php if(have_posts()) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
<div class="container ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 responsive-con">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
             <div class="img-responsive"><?php the_post_thumbnail(); ?></div>
            <strong class="img-low-text"><?php the_title();  ?></strong>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 respon-padd">
            <img src="<?php bloginfo("template_directory");?>/img/cc.png" alt="" class="img-responsive">
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div style="font-weight: 700;"><?php the_content(); ?></div>
                <br>
                <strong class="responsive-low-text"><?php echo (get_post_meta($post->ID, 'name', true)); ?></strong>
        </div>
    </div>
</div>
   <?php endwhile; ?>
        <?php else : ?>
        <h2>Записей нет</h2>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
<?php wp_footer(); ?>
<?php get_footer('page'); ?>