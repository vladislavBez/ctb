<?php
/*
Template Name: Страница с контентом
*/
include 'header-page.php';
?>

<!-- Modal -->
<div class="modal fade" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Подобрать тур</h4></center>
                </div>
                <div class="modal-body">
                    <form action="https://ctb76.ru/podbor.php" class="contact" method="post" name="test" id="form">
                        <label for="name">ФИО:</label>
                        <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name" class="form-control" required="required"></p>
                        <label for="phone">Телефон:</label>
                        <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone" class="form-control" required="required"></p>
                        <label for="email">E-mail:</label>
                        <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail" class="form-control" required="required"></p>
                        <label for="message">Сообщение:</label><br/>
                        <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
						<label for="name" style="color: black;">Введите цифры с картинки:</label>
                        <!-- вывод капчи из файла captcha.php -->
						<p><?php require ("captcha.php"); ?></p>
                        <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                        <center><input type="submit" class="btn btn-lg red"  id="submit" name="form" value="Отправить"></center>
                    </form>
                </div>
            </div>
        </div>
 </div>
   <div class="container">
   <div class="col-lg-12 col-md-12 col-xs-12">
  <div class="row">
          <div id="primary" class="site-content">
               <div id="content" role="main">

                <?php while ( have_posts() ) : the_post(); ?>
              <div class="entry-header">
                <?php the_post_thumbnail(); ?>
                <h1 class="gold"><?php the_title(); ?></h1>
             </div>
               <div class="entry-content">
                <?php the_content(); ?>
               </div><!-- .entry-content --> 
               <?php comments_template( '', true ); ?>
               <?php endwhile; // end of the loop. ?>
    
               </div><!-- #content -->
           </div><!-- #primary -->
	 
	    </div>
		</div>
   </div>
<footer class="footer_page-content">
    <div class="container">
        <!--Нижняя часть с логотимом и надписями-->
             <div class="row padd-bot">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 big-img">
                    <img class="img-responsive" src="<?php bloginfo("template_directory");?>/img/big.png" alt="">
                </div>
                <div class="">
                   <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 ">
                         <ul>
                           <li><a href="<?php echo get_permalink(118); ?>">Фото и Видео</a></li>
                           <li><a href="<?php echo get_permalink(90); ?>">Поиск тура</a></li>
                           <li><a href="<?php echo get_permalink(92); ?>">Горящие</a></li>
                           <li><a href="<?php echo get_permalink(95); ?>">Раннее Бронирование</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <ul>
                            <li><a href="<?php echo get_permalink(97); ?>">Акции и Бонусы</a></li>
                            <li><a href="<?php echo get_permalink(100); ?>">Отзывы</a></li>
                            <li><a href="<?php echo get_permalink(103); ?>">Услуги</a></li>
                            <li><a href="<?php echo get_permalink(105); ?>">Программа привилегий</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--Самый низ-->
    </div>
<?php wp_footer(); ?>
<?php get_footer('page'); ?>