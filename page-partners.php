<?php
/*
Template Name: Страница Партнеры
*/
include 'header-page.php';
?>
<!-- Modal -->
<div class="modal fade" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Подобрать тур</h4></center>
                </div>
                <div class="modal-body">
                    <form action="https://ctb76.ru/podbor.php" class="contact" method="post" name="test" id="form">
                        <label for="name">ФИО:</label>
                        <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name" class="form-control" required="required"></p>
                        <label for="phone">Телефон:</label>
                        <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone" class="form-control" required="required"></p>
                        <label for="email">E-mail:</label>
                        <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail" class="form-control" required="required"></p>
                        <label for="message">Сообщение:</label><br/>
                        <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
						<label for="name" style="color: black;">Введите цифры с картинки:</label>
                        <!-- вывод капчи из файла captcha.php -->
						<p><?php require ("captcha.php"); ?></p>
                        <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                        <center><input type="submit" class="btn btn-lg red"  id="submit" name="form" value="Отправить"></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
<!-- Modal -->
<?php wp_footer(); ?>
<body>
<div class="container-fluid banner-partners">

    <div class="container">
        <h1 class="white event-h1">ПАРТНЁРЫ</h1>
        <h2 class="second-title gold">РАБОТАЕМ ТОЛЬКО С ПРОВЕРЕННЫМИ ТУРОПЕРАТОРАМИ</h2>
    </div>

</div>
<div class="container-fluid">
    <div class="container cash-padd-4">
        <!--первый ряд-->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_BIBGLO.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_ANEX.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_VIking.png" alt="" class="img-responsive center-block ">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_Pegas.png" alt="" class="img-responsive center-block">
            </div>
        </div>
        <!--второй ряд-->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_Troyka.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_SanMar.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_Coral.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_TezT.png" alt="" class="img-responsive center-block">
            </div>
        </div>
        <!--третий ряд-->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_IntTur.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_Allean.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_Mouze.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_Tui.png" alt="" class="img-responsive center-block">
            </div>
        </div>
        <!--четвёртый ряд-->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_Paks.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_RusEx.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_AMBOTIS.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_DVM_T.png" alt="" class="img-responsive center-block">
            </div>
        </div>
        <!--пятый ряд-->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_TTVoyage.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_Dolph.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_ICS.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_AMIGOS.png" alt="" class="img-responsive center-block">
            </div>
        </div>
        <!--шестой ряд-->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_AMIGOTU.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_ANKOR.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_BALKAN.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_Danko.png" alt="" class="img-responsive center-block">
            </div>
        </div>
        <!--седьмой ряд-->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_WEST.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_Multi.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_RASGr.png" alt="" class="img-responsive center-block">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padding-partners">
                <img src="<?php bloginfo("template_directory");?>/img/logo-partners/CTB_sait_logo_VEDI.png" alt="" class="img-responsive center-block">
            </div>
        </div>
    </div>
</div>
<?php wp_footer(); ?>
<?php get_footer('page'); ?>
