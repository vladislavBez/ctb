<?php
/*
Template Name: Страница Программа привилегий
*/
include 'header-page.php';
?>
<!-- Modal -->
<div class="modal fade" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Подобрать тур</h4></center>
                </div>
                <div class="modal-body">
                    <form action="https://ctb76.ru/podbor.php" class="contact" method="post" name="test" id="form">
                        <label for="name">ФИО:</label>
                        <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name" class="form-control" required="required"></p>
                        <label for="phone">Телефон:</label>
                        <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone" class="form-control" required="required"></p>
                        <label for="email">E-mail:</label>
                        <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail" class="form-control" required="required"></p>
                        <label for="message">Сообщение:</label><br/>
                        <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
						<label for="name" style="color: black;">Введите цифры с картинки:</label>
                        <!-- вывод капчи из файла captcha.php -->
						<p><?php require ("captcha.php"); ?></p>
                        <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                        <center><input type="submit" class="btn btn-lg red"  id="submit" name="form" value="Отправить"></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="myModal-9" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">ПОЛУЧИТЬ КАРТУ</h4></center>
                </div>
                <div class="modal-body">
                    <form action="http://ctb76.ru/card.php/" class="contact" method="post" name="test" id="form">
                        <label for="name">ФИО:</label>
                        <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name" class="form-control" required="required"></p>
                        <label for="phone">Телефон:</label>
                        <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone" class="form-control" required="required"></p>
                        <label for="email">E-mail:</label>
                        <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail" class="form-control" required="required"></p>
                        <label for="message">Сообщение:</label><br/>
                        <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
						<label for="name" style="color: black;">Введите цифры с картинки:</label>
                        <!-- вывод капчи из файла captcha.php -->
						<p><?php require ("captcha.php"); ?></p>
                        <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                        <center><input type="submit" class="btn btn-lg red"  id="submit" name="form" value="Отправить"></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
<!-- Modal -->
<body>
<div class="container-fluid vip-banner">

    <div class="container">
        <h1 class="white event-h1">ПРОГРАММА ПРИВИЛЕГИЙ "ПЯТЬ ЗВЕЗД"</h1>
        <p class="second-title gold">БОЛЬШЕ ПУТЕШЕСТВИЙ - БОЛЬШЕ ПРИВИЛЕГИЙ</p>
    </div>
</div>
<div class="container text-center team-padd">
    <strong class="text-size-7 broun">КАРТА ТУРИСТА</strong><br>
    <p>Наша программа привилегий включает 5 статусов, в зависимости <br> от количества и частоты путешествий.</p>
    <img src="<?php bloginfo("template_directory");?>/img/card-2.png" class="img-responsive" alt="">

</div>
<div class="container-fluid vip-banner-2 text-center">
    <h2 class="gold text-size-5 about-strong">КАК ПОТРАТИТЬ БОНУСЫ?</h2>
    <div class="container">
<!--        первая строка-->
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
            <img class="img-responsive center-block" src="<?php bloginfo("template_directory");?>/img/rger.png" alt="">
            <div class="about-strong ">
                <strong class="gold font-size-2">СУВЕНИРЫ</strong>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <img class="img-responsive center-block" src="<?php bloginfo("template_directory");?>/img/ewetpng.png" alt="">
            <div class="about-strong ">
                <strong class="gold font-size-2">СКИДКИ НА ТУР</strong>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <img class="img-responsive center-block" src="<?php bloginfo("template_directory");?>/img/12132t.png" alt="">
            <div class="about-strong ">
                <strong class="gold font-size-2">ПОДАРОЧНЫЕ СЕРТИФИКАТЫ</strong>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <img class="img-responsive center-block" src="<?php bloginfo("template_directory");?>/img/gewe.png" alt="">
            <div class="about-strong ">
                <strong class="gold font-size-2">ТРАНСФЕРЫ</strong>
            </div>
        </div>
<!--        вторая строка-->
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <img class="img-responsive center-block" src="<?php bloginfo("template_directory");?>/img/sgsrgh.png" alt="">
            <div class="about-strong ">
                <strong class="gold font-size-2">ПОДАРКИ ОТ ПАРТНЕРОВ</strong>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <img class="img-responsive center-block" src="<?php bloginfo("template_directory");?>/img/drhedrh.png" alt="">
            <div class="about-strong ">
                <strong class="gold font-size-2">ЗАКРЫТЫЕ<br> МЕРОПРИЯТИЯ <br> ДЛЯ КЛИЕНТОВ</strong>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <img class="img-responsive center-block" src="<?php bloginfo("template_directory");?>/img/rgeh.png" alt="">
            <div class="about-strong ">
                <strong class="gold font-size-2">СТРАХОВАНИЕ</strong>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd-bott">
            <button data-toggle="modal" data-target="#myModal-9" type="button" class="btn btn-lg red marg">ПОЛУЧИТЬ КАРТУ</button>	
				<form action="https://ctb76.ru/wp-content/uploads/2018/04/PP_na_sayt.pdf" target="_blank">
                    <button type="submit" class="btn btn-lg red">ПОДРОБНЕЕ О ПРОГРАММЕ</button>
					</form>	
		
        </div>
    </div>
</div>
	<?php wp_footer(); ?>
<?php get_footer('page'); ?>