<?php
/*
Template Name: Страница О нас
*/
include 'header-page.php';
?>
<!-- Modal -->
<div class="modal fade" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Подобрать тур</h4></center>
                </div>
                <div class="modal-body">
                    <form action="https://ctb76.ru/podbor.php" class="contact" method="post" name="test" id="form">
                        <label for="name">ФИО:</label>
                        <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name" class="form-control" required="required"></p>
                        <label for="phone">Телефон:</label>
                        <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone" class="form-control" required="required"></p>
                        <label for="email">E-mail:</label>
                        <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail" class="form-control" required="required"></p>
                        <label for="message">Сообщение:</label><br/>
                        <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
						<label for="name" style="color: black;">Введите цифры с картинки:</label>
                        <!-- вывод капчи из файла captcha.php -->
						<p><?php require ("captcha.php"); ?></p>
                        <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                        <center><input type="submit" class="btn btn-lg red"  id="submit" name="form" value="Отправить"></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
<!-- Modal -->
<body>
<div class="container-fluid about-banner">
    <div class="container">
        <h1 class="white event-h1">О НАС</h1>
    </div>
</div>
<div class="container-fluid">
    <div class="container container-about">
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
            <img src="<?php bloginfo("template_directory");?>/img/about-text.png" alt="" class="img-responsive">
        </div>
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
            <p>Имея большой опыт в индустрии туризма, мы применяем современные маркетинговые инструменты в работе с
                клиентами и партнерами. Используя широкий спектр каналов привлечения новых клиентов, мы эффективно
                работаем над тем, чтобы они стали нашими постоянными туристами.</p>
            <p>И это у нас получается: общая база наших клиентов уже насчитывает более 20 000 человек и регулярно
                увеличивается. Залог успеха нашего дела – это, конечно, команда профессионалов! Молодой и дружный
                коллектив нашей компании всегда открыт к новым идеям и нестандартным подходам.</p>
            <p>Мы не просто работаем вместе, мы легко можем за пару дней собрать чемоданы и махнуть на лазурный берег в
                поисках приключений!</p>
        </div>
    </div>
</div>
<div class="container-fluid about-banner-why">
    <div class="container text-center">
        <h2 class="gold text-size-5">ПОЧЕМУ МЫ ?</h2>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <img class="img-responsive center-block" src="<?php bloginfo("template_directory");?>/img/about-1.png" alt="">
            <div class="about-strong ">
                <strong class="gold font-size-2">ПРОФЕССИОНАЛЬНО</strong>
            </div>
            <p class="white font-size-2 ">Наши специалисты работают в сфере туризма более 15 лет.</p>
            <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-2 col-sm-offset-5 col-xs-2 col-xs-offset-5 about-border about-am-1"></div>
        </div>
        <div class="col-lg-3  col-md-3 col-sm-12 col-xs-12">
            <img class="img-responsive center-block" src="<?php bloginfo("template_directory");?>/img/about-2.png" alt="">
            <div class="about-strong about-am-3">
                <strong class="gold font-size-2">ЛЮБАЯ ФОРМА ОПЛАТЫ</strong>
            </div>
            <p class="white font-size-2">Возможность оплаты по карте и рассрочка до 4 месяцев</p>
            <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-2 col-sm-offset-5 col-xs-2 col-xs-offset-5 about-border about-am-2"></div>
        </div>
        <div class="col-lg-3  col-md-3 col-sm-12 col-xs-12">
            <img class="img-responsive center-block" src="<?php bloginfo("template_directory");?>/img/about-3.png" alt="">
            <div class="about-strong ">
                <strong class="gold font-size-2">НАДЕЖНО</strong>
            </div>
            <p class="white font-size-2 ">Работаем только с проверенными туроператорами</p>
            <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-2 col-sm-offset-5 col-xs-2 col-xs-offset-5 about-border"></div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <img class="img-responsive center-block" src="<?php bloginfo("template_directory");?>/img/about-4.png" alt="">
            <div class="about-strong ">
                <strong class="gold font-size-2">ЛУЧШАЯ ЦЕНА</strong>
            </div>
            <p class="white font-size-2 ">Работаем только по спецпредложениям и выгодным ценам</p>
            <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-2 col-sm-offset-5 col-xs-2 col-xs-offset-5 about-border"></div>
        </div>
    </div>
</div>
<div class="container-fluid padd-bott">
    <div class="text-center banner-lvl-4-padd">
        <strong class="text-size-7">НАША КОМАНДА</strong>
    </div>
    <div class="container about-person">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 personal-cart">
            <div class="col-lg-3 col-md-3 col-sm-3 col-sx-3">
                <img class="img-responsive" src="<?php bloginfo("template_directory");?>/img/workers/Popova-person.jpg" alt="">
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 about-person-text">
                <STRONG class="name">ЮЛИЯ ПОПОВА</STRONG>
                <p class="grey">Генеральный директор</p>
                <p>Основатель, лидер и душа компании. Красный диплом с отличием профильного ВУЗа и более, чем 15 лет в
                    туризме. Огромный опыт, знание многочисленной клиентской базы в лицо; чуткое отношение к клиентам,
                    коллегам и подчинённым; неуёмное желание достичь совершенства во всех аспектах деятельности
                    турагентсва.</p>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-sx-12 personal-cart">
            <div class="col-lg-3 col-md-3 col-sm-3 col-sx-3">
                <img class="img-responsive" src="<?php bloginfo("template_directory");?>/img/workers/Morozova-person.jpg" alt="">
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-sx-9 about-person-text">
                <STRONG class="name">ОЛЬГА МОРОЗОВА</STRONG>
                <p class="grey">Директор по развитию</p>
                <p>За шесть лет работы менеджером по продажам и ведущим менеджером, шагнула на новую ступень. Знает
                    специфику работы изнутри. Имеет большой опыт по организации и продвижению крупных федеральных
                    проектов. В курсе всех основных трендов индустрии.
                </p>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-sx-12 personal-cart">
            <div class="col-lg-3 col-md-3 col-sm-3 col-sx-3">
                <img class="img-responsive" src="<?php bloginfo("template_directory");?>/img/workers/Yakovleva-person.jpg" alt="">
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-sx-9 about-person-text">
                <STRONG class="name">АННА ЯКОВЛЕВА</STRONG>
                <p class="grey">Менеджер по выездному туризму и индивидуальным турам </p>
                <p>Прошла обучение в профильном ВУЗе, имеет за спиной более 20 стран, в которых лично побывала в качестве туриста. Регулярно проходит тренинги по технологиям работы с клиентами и профильные обучающие программы. Неоднократно была удостоена звания лучший менеджер месяца.
                </p>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-sx-12 personal-cart">
            <div class="col-lg-3 col-md-3 col-sm-3 col-sx-3">
                <img class="img-responsive" src="<?php bloginfo("template_directory");?>/img/workers/Gribanova-person.jpg" alt="">
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-sx-9 about-person-text">
                <STRONG class="name">ЛЮБОВЬ ГРИБАНОВА</STRONG>
                <p class="grey">Менеджер по выездному туризму и индивидуальным турам </p>
                <p>Живой, общительный, по-хорошему въедливый, любящий свою работу специалист. На постоянной основе повышает свой профессионализм, изучая как самые популярные страны и курорты, так и молодые, развивающиеся направления. Имеет в своей копилке множество направлений, где побывала в рекламных турах и на отдыхе.
                </p>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-sx-12 personal-cart">
            <div class="col-lg-3 col-md-3 col-sm-3 col-sx-3">
                <img class="img-responsive" src="<?php bloginfo("template_directory");?>/img/workers/Musinova-person.jpg" alt="">
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-sx-9 about-person-text">
                <STRONG class="name">ИРИНА МУСИНОВА</STRONG>
                <p class="grey">Менеджер по выездному туризму и индивидуальным турам </p>
                <p >Ответственная, клиенториетированная, располагающая к себе любого клиента. Активная жизненная позиция и стремление к постоянному саморазвитию. Обладает обширными знаниями в области туризма, до мельчайших деталей может рассказать о специфике отдыха на том или ином курорте.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid contacst-about">
    <div class="container about-padd-con">
	 <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <!--Блок с контактами-->
            <div class="row">
                <strong class="gold contacts-h1"><strong>КОНТАКТЫ</strong></strong><br>
                <strong class="white font-size-2">ООО "ЦЕНТР ТУРИСТИЧЕСКОГО БРОНИРОВАНИЯ"</strong>

            </div>
            <div class="row footer-padding choice-button-3">
                <strong class="gold"><strong>АДРЕС:</strong></strong>
                <p>г. Ярославль, ул. Победы, д. 43/61</p> 
                <p>ТЦ "МИГ" (у Макдональдса), 2-ой этаж</p>
            </div>
            <div class="row footer-padding">
                <strong class="gold"><strong>ГРАФИК РАБОТЫ:</strong></strong>
                <p>Пн-Пт: 10:00 - 19:00</p>
                <p>Сб-Вс: выходной</p>
            </div>
            <div class="row footer-padding">
                <strong class="gold"><strong>E-MAIL:</strong></strong>
                <p>info@ctb76.ru</p>
            </div>
            <div class="row footer-padding">
                <strong class="gold"><strong>ТЕЛЕФОН:</strong></strong>
                <p>+7 (4852) 94-14-50, +7 (909) 280-90-55, +7 (920) 118-20-57</p>
            </div>
            <div class="row footer-padding">
                <strong class="gold"><strong>WHATSAPP, VIBER:</strong></strong>
                <p>+7 (909) 280-90-55</p>
            </div>
            <div class="row page-footer-soc">
                <h4 class="gold"><strong>СОЦИАЛЬНЫЕ СЕТИ</strong></h4>
                <a class="footer_social" role="button" href="https://vk.com/ctb76" target="_blank" title="Телефон ЦТБ">
                    <img class=""
                         src="<?php bloginfo("template_directory"); ?>/img/svg/vk.svg"
                         alt="Мы Вконтакте">
                </a>
                <a class="footer_social" href="https://www.instagram.com/ctb76.ru/" target="_blank">
                    <img class=""
                         src="<?php bloginfo("template_directory"); ?>/img/svg/instagram.svg"
                         alt="Мы в Instagram">
                </a>
                <a class="footer_social" href="https://www.facebook.com/groups/584779218582089/" target="_blank">
                    <img class=""
                         src="<?php bloginfo("template_directory"); ?>/img/svg/facebook.svg"
                         alt="Мы в Facebook">
                </a>
                <a class="footer_social" href="https://ok.ru/group/55405382270984" target="_blank">
                    <img class=""
                         src="<?php bloginfo("template_directory"); ?>/img/svg/odnoklassniki-logo (1).svg"
                         alt="Мы в Одноклассники">
                </a>
            </div>

        </div>
        <div class="col-md-6 col-lg-6 hidden-xs hidden-sm">
            <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Afd718fa5b02b5fb71a15f22fec2592ab39146bc79d0aec7366bb31d0f77da6c5&amp;source=constructor" width="100%" height="450" frameborder="0"></iframe>
        </div>
    </div>
		 </div>
</div>
	<?php wp_footer(); ?>
<?php get_footer('page'); ?>
