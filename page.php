<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ctb
 */
get_header(); ?>
    <!--Modal-->
    <body>
    <div class="modal fade" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Подобрать тур</h4></center>
                </div>
                <div class="modal-body">
                    <form action="https://ctb76.ru/podbor.php" class="contact" method="post" name="test" id="form">
                        <label for="name">ФИО:</label>
                        <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name"
                                  class="form-control" required="required"></p>
                        <label for="phone">Телефон:</label>
                        <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone"
                                  class="form-control" required="required"></p>
                        <label for="email">E-mail:</label>
                        <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail"
                                  class="form-control" required="required"></p>
                        <label for="message">Сообщение:</label><br/>
                        <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
                        <label for="name" style="color: black;">Введите цифры с картинки:</label>
                        <!-- вывод капчи из файла captcha.php -->
                        <p><?php require("captcha.php"); ?></p>
                        <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                        <center><input type="submit" class="btn btn-lg red" id="submit" name="form" value="Отправить">
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--End-Modal-->
    <div class="container-fluid banner">
        <div id="target" class="parallax-port hidden-sm hidden-xs">
            <img class="parallax-layer" src="<?php bloginfo("template_directory"); ?>/img/target_blue4.png"
                 alt="ПОИСК ТУРА"/>
            <img class="parallax-layer paralax-text img-responsive"
                 src="<?php bloginfo("template_directory"); ?>/img/target_green4.png" alt="ПОИСК ТУРА"/>
            <img class="parallax-layer" src="<?php bloginfo("template_directory"); ?>/img/target_red4.png"
                 alt="ПОИСК ТУРА"/>
        </div>
        <!--Заголовок баннера-->
        <div class="container banner-top hidden-xs">
            <!--Блоки с картинками-->
            <div class="col-lg-4 banner-1-icon  col-md-4 col-sm-4 col-xs-4"><a href="//ctb76.ru/goryashhie/"
                                                                               class="bann-icon"><img
                            class="img-responsive" src="<?php bloginfo("template_directory"); ?>/img/ico-1.png"
                            alt="Горящие Туры"><span class="icon-text">Горящие</span></a></div>
            <div class="col-lg-4 banner-1-icon col-md-4 col-sm-4 col-xs-4"><a href="<?php echo get_permalink(95); ?>"
                                                                              class="bann-icon"><img
                            class="img-responsive" src="<?php bloginfo("template_directory"); ?>/img/ico-2.png"
                            alt="Раннее Бронирование"><span class="icon-text">Раннее Бронирование</span></a></div>
            <div class="col-lg-4 banner-1-icon col-md-4 col-sm-4 col-xs-4"><a href="//ctb76.ru/zakazat-transfer/"
                                                                              class="bann-icon"><img
                            class="img-responsive" src="<?php bloginfo("template_directory"); ?>/img/ico-3.png"
                            alt="Заказать трансфер"><span class="icon-text">Заказать трансфер</span></a></div>
        </div>
    </div>
    <a name="tur"></a>
    <h1 class="font-h1">Центр Туристического Бронирования - Поиск Тура | Подбор Тура</h1>
    <div class="container-fluid fraim">
        <div class="container">
            <div class="row">
                <script type="text/javascript" src="//ui.sletat.ru/module-5.0/app.js" charset="utf-8"></script>
                <script type="text/javascript">sletat.createModule5('Search', {
                        files: ["//ui.sletat.ru/module-5.0/theme/sea_dec2015.min.css"],
                        files: ["<?php bloginfo("template_directory");?>/dist/css/fraim.css"],
                        enabledCurrencies: ["RUB", "EUR", "USD"],
                        useCard: false,
                    });</script>
                <span class="sletat-copyright">Идет загрузка модуля <a href="//sletat.ru/" title="поиск туров"
                                                                       target="_blank">поиска туров</a> &hellip;</span>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Карусель -->
        <div id="myCarousel-1" class="carousel slide hidden-xs" data-interval="5000" data-ride="carousel">
            <!-- Индикаторы для карусели -->
            <div class="carousel-indicators lvl-2-carusel-indicators">
                <a style="margin-right: -0.5rem;" data-target="#myCarousel-1" data-slide="prev">
                    <span class="glyphicon"><img src="<?php bloginfo("template_directory"); ?>/img/arro-left.png"
                                                 alt=""></span>
                </a>
                <a data-target="#myCarousel-1" data-slide="next">
                    <span class="glyphicon"><img src="<?php bloginfo("template_directory"); ?>/img/arro-right.png"
                                                 alt=""></span>
                </a>
            </div>
            <!-- Слайды карусели -->
            <div class="carousel-inner">
                <!-- Слайды создаются с помощью контейнера с классом item, внутри которого помещается содержимое слайда -->
                <div class="active item">
                    <img class="" src="<?php bloginfo("template_directory"); ?>/img/slide-1.png"
                         alt="Наши специалисты работают в сфере туризма более 15 лет">
                </div>
                <!-- Слайд №2 -->
                <div class="item">
                    <img class="" src="<?php bloginfo("template_directory"); ?>/img/slide-2.png"
                         alt="Возможность оплаты по карте и рассрочка до 4 месяцев">
                </div>
                <!-- Слайд №3 -->
                <div class="item">
                    <img class="" src="<?php bloginfo("template_directory"); ?>/img/slide-3.png"
                         alt="Работаем только с проверенными туроператорами">
                </div>
                <div class="item">
                    <img class="" src="<?php bloginfo("template_directory"); ?>/img/slide-4.png"
                         alt="Работаем только по спецпредложениям и выгодным ценам">
                </div>
            </div>
        </div>
        <div class="Mobile-slick-carusel visible-xs">
            <div>
                <img class="img-responsive" src="<?php bloginfo("template_directory"); ?>/img/slide-smal-1.png"
                     alt="Наши специалисты работают в сфере туризма более 15 лет">
            </div>
            <div>
                <img class="img-responsive" src="<?php bloginfo("template_directory"); ?>/img/slide-smal-3.png"
                     alt="Работаем только с проверенными туроператорами">
            </div>
            <div>
                <img class="img-responsive" src="<?php bloginfo("template_directory"); ?>/img/slide-smal-4.png"
                     alt="Работаем только по спецпредложениям и выгодным ценам">
            </div>
        </div>
        <!--Блок с девочкой-->
        <div class="container-fluid girl-banner">
            <div class="row">
                <div class="container banner-lvl-4-padd">
                    <div class="col-lg-8"></div>
                    <div class="col-lg-3 girl-aligh">
                        <h2><strong class="strong white">ЗАКАЖИ ПОДБОР ТУРА</strong></h2>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
            </div>
            <div class="row">
                <div class="container">
                    <div class="col-lg-8 col-md-5 col-sm-4 hidden-xs"></div>
                    <div class="col-lg-3 col-md-4 col-sm-1 col-xs-12 xs-flex banner-lvl-4-button-padd center-block">
                        <button type="button" data-toggle="modal" data-target="#myModal-1" class="btn btn-lg red">
                            ЗАКАЗАТЬ
                        </button>
                    </div>
                    <div class="col-lg-1 "></div>
                </div>
                <div class="modal fade" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                                <center><h4 class="modal-title" style="color: black;" id="myModalLabel">Заказать подбор
                                        тура</h4></center>
                            </div>
                            <div class="modal-body">
                                <form action="https://ctb76.ru/podbor.php" class="contact" method="post" name="test"
                                      id="form">
                                    <label for="name" style="color: black;">ФИО:</label>
                                    <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name"
                                              class="form-control" required="required"></p>
                                    <label for="phone" style="color: black;">Телефон:</label>
                                    <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон"
                                              id="phone" class="form-control" required="required"></p>
                                    <label for="email" style="color: black;">E-mail:</label>
                                    <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail"
                                              class="form-control" required="required"></p>
                                    <label for="message" style="color: black;">Сообщение:</label><br/>
                                    <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
                                    <label for="name" style="color: black;">Введите цифры с картинки:</label>
                                    <!-- вывод капчи из файла captcha.php -->
                                    <p><?php require("captcha.php"); ?></p>
                                    <input name="captcha_validation" type="text" size="6" maxlength="5"
                                           required="required"><br/>
                                    <center><input type="submit" class="btn btn-lg red" id="submit" name="form"
                                                   value="Заказать"></center>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Контейнер с партнёрами-->
        <div class="container-fluid partners-con">
            <div class="container">
                <div class="row lvl-5-parners-padding">
                    <!--Ряд с названием-->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                        <h2><strong>ПАРТНЁРЫ</strong></h2>
                    </div>
                </div>
                <!--Ряд с 4мя нартнёрами-->
                <div class="row">

                    <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 partners-logo vcenter">

                        <img class="img-responsive center-block"
                             src="<?php bloginfo("template_directory"); ?>/img/partners-anex.png" alt="">

                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 partners-logo vcenter">

                        <img class="img-responsive center-block"
                             src="<?php bloginfo("template_directory"); ?>/img/partners-biblio.png" alt="">

                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 partners-logo vcenter">

                        <img class="img-responsive center-block"
                             src="<?php bloginfo("template_directory"); ?>/img/partners-pegas.png" alt="">

                    </div>

                    <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 partners-logo vcenter">

                        <img class="img-responsive center-block"
                             src="<?php bloginfo("template_directory"); ?>/img/Alean.png" alt="">

                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 hidden-sm hidden-xs partners-logo vcenter">

                        <img class="img-responsive center-block"
                             src="<?php bloginfo("template_directory"); ?>/img/logo-partners/CTB_sait_logo_VIking.png"
                             alt="">

                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 hidden-sm hidden-xs partners-logo vcenter">

                        <img class="img-responsive center-block"
                             src="<?php bloginfo("template_directory"); ?>/img/Koral_trevel.png" alt="">

                    </div><!---->
                    <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 hidden-lg hidden-sm hidden-xs partners-logo vcenter">

                        <img class="img-responsive center-block"
                             src="<?php bloginfo("template_directory"); ?>/img/Muzenidis.png" alt="">

                    </div><!---->
                    <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 hidden-lg hidden-sm hidden-xs partners-logo vcenter">

                        <img class="img-responsive center-block"
                             src="<?php bloginfo("template_directory"); ?>/img/Sanmar.png" alt="">
                    </div>
                </div>
                <!--Ряд с кнопкой-->
                <div class="row ">
                    <div class="col-lg-12 text-center lvl-5-parners-padding">
                        <form action="//ctb76.ru/partners/">
                            <button type="submit" class="btn btn-banner btn-partners"><strong>ВСЕ ПАРТНЁРЫ</strong>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--Контейнер с командой-->
        <div class="container-fluid"
             style="background-image: url(<?php bloginfo("template_directory"); ?>/img/background-undo.png);">
            <div class="container hidden-xs">
                <div class="row team-padd">
                    <h2 class="white text-center"><strong>НАША КОМАНДА</strong></h2>
                </div>
                <div class="row team-padd">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="thumbnail">
                            <img src="<?php bloginfo("template_directory"); ?>/img/workers/Popova-logo.jpg" alt="...">
                            <div class="caption">
                                <h3 class="caption-header">Юлия Попова</h3>
                                <p>Генеральный директор</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="thumbnail">
                            <img src="<?php bloginfo("template_directory"); ?>/img/workers/Morozova-logo.jpg" alt="...">
                            <div class="caption">
                                <h3 class="caption-header">Ольга Морозова</h3>
                                <p>Директор по развитию</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="thumbnail">

                            <img src="<?php bloginfo("template_directory"); ?>/img/workers/Yakovleva-logo.jpg"
                                 alt="...">

                            <div class="caption">
                                <h3 class="caption-header">Анна Яковлева</h3>
                                <p>Менеджер по выездному туризму и индивидуальным турам</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row team-padd">
                    <div class="col-lg-2 col-md-2 col-sm-2"></div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="thumbnail">
                            <img src="<?php bloginfo("template_directory"); ?>/img/workers/Gribanova-logo.jpg"
                                 alt="...">
                            <div class="caption">
                                <h3 class="caption-header">Любовь Грибанова</h3>
                                <p>Менеджер по выездному туризму и групповым турам</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="thumbnail">
                            <img src="<?php bloginfo("template_directory"); ?>/img/workers/Musinova-logo.jpg" alt="...">
                            <div class="caption">
                                <h3 class="caption-header">Ирина Мусинова</h3>
                                <p>Менеджер по внутреннему и выездному туризму</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
            <div class="row team-padd visible-xs">
                <h2 class="white text-center"><strong>НАША КОМАНДА</strong></h2>
            </div>
            <div class="team-slider visible-xs">
                <div>
                    <div class="thumbnail">
                        <img src="<?php bloginfo("template_directory"); ?>/img/workers/Popova-logo.jpg" alt="...">
                        <div class="caption">
                            <h3 class="caption-header">Юлия Попова</h3>
                            <p>Генеральный директор</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="thumbnail">
                        <img src="<?php bloginfo("template_directory"); ?>/img/workers/Morozova-logo.jpg" alt="...">
                        <div class="caption">
                            <h3 class="caption-header">Ольга Морозова</h3>
                            <p>Директор по развитию</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="thumbnail">
                        <img src="<?php bloginfo("template_directory"); ?>/img/workers/Yakovleva-logo.jpg"
                             alt="...">
                        <div class="caption">
                            <h3 class="caption-header">Анна Яковлева</h3>
                            <p>Менеджер по выездному туризму и индивидуальным турам</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="thumbnail">
                        <img src="<?php bloginfo("template_directory"); ?>/img/workers/Gribanova-logo.jpg"
                             alt="...">
                        <div class="caption">
                            <h3 class="caption-header">Любовь Грибанова</h3>
                            <p>Менеджер по выездному туризму и групповым турам</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="thumbnail">
                        <img src="<?php bloginfo("template_directory"); ?>/img/workers/Musinova-logo.jpg" alt="...">
                        <div class="caption">
                            <h3 class="caption-header">Ирина Мусинова</h3>
                            <p>Менеджер по внутреннему и выездному туризму</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-fluid">
            <div class="row lvl-5-parners-padding cash-padd-4">
                <!--Отзывы туристов-->
                <h2 class="white text-center "><strong>ОТЗЫВЫ ТУРИСТОВ</strong></h2>
            </div>
            <!--Карусель на дектопе-->
            <div class="row">
                <div class="response-slider">
                    <div>
                        <div class="container carusel-4">
                            <div class="col-lg-2 col-md-2 col-sm-1 hidden-xs"></div>
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 carusel-4-col">
                                <img class="img-responsive center-block"
                                     src="<?php bloginfo("template_directory"); ?>/img/turciy-img.png" alt="">
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 carusel-4-col">
                                <img class="img-responsive center-block"
                                     src="<?php bloginfo("template_directory"); ?>/img/quates.png" alt="">
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 carusel-4-col">
                                <p>Турция, октябрь 2017 г.<br/>
                                    Отдыхали в Турции ( Кемер ), туроператор TUI. Жили в отеле Asdem park 4*. В
                                    отеле не первый раз. Очень удобное расположение - практически центр города.
                                    Вторая линия, есть собственный пляж. Отель очень молодежный, шумный.
                                    Дискотеки и шоу программа каждый вечер. Есть второй корпус- более тихий.
                                    Гости отеля - русские, турки. Питание соответствует 4-м звёздам отеля<a
                                            href="<?php echo get_permalink(100); ?>">...</a></p>
                                <div class="author">Наталья Вишневская</div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs"></div>
                        </div>
                    </div>
                    <div>
                        <div class="container carusel-4">
                            <div class="col-lg-2 col-md-2 col-sm-1 hidden-xs"></div>
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 carusel-4-col">
                                <img class="img-responsive center-block"
                                     src="<?php bloginfo("template_directory"); ?>/img/krabi-img.png" alt="">
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 carusel-4-col">
                                <img class="img-responsive center-block"
                                     src="<?php bloginfo("template_directory"); ?>/img/quates.png" alt="">
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 carusel-4-col">
                                <p>Тайланд, Краби, январь 2018 г.<br/>
                                    Место очень крутое: в распоряжении несколько пляжей в шаговой доступности, по
                                    пути вы обязательно встретите представителей местной фауны (обезьяны, вараны,
                                    колибри и даже белочек). Самостоятельно можно посетить смотровые площадки,
                                    лагуны, пещеры (не выезжая с острова и бесплатно). Для экстрималов огромный
                                    выбор мест и средств для скалолазания.<a
                                            href="<?php echo get_permalink(100); ?>">...</a></p>
                                <div class="author">Виктория Соколова</div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs"></div>
                        </div>
                    </div>
                    <div>
                        <div class="container carusel-4">
                            <div class="col-lg-2 col-md-2 col-sm-1 hidden-xs"></div>
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 carusel-4-col">
                                <img class="img-responsive center-block"
                                     src="<?php bloginfo("template_directory"); ?>/img/tunis-img.png" alt="">
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 carusel-4-col">
                                <img class="img-responsive center-block"
                                     src="<?php bloginfo("template_directory"); ?>/img/quates.png" alt="">
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 carusel-4-col">
                                <p>Тунис, октябрь 2017 г.<br/>
                                    Была наслышана о том, что Тунис очень грязная страна – это, конечно, смущало
                                    перед выбором направления. Но девочки в агентстве смоги переубедить, мы с семьей
                                    летаем от них уже не в первый раз. Доброжелательное отношение сотрудников,
                                    всегда есть ответы на все вопросы. Отдельное спасибо за оперативность выбора
                                    отеля – мы были в Монастире отель Delphin Habib 4*.<a
                                            href="<?php echo get_permalink(100); ?>">...</a></p>
                                <div class="author">Наталья Сухова</div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs"></div>
                        </div>
                    </div>
                    <div>
                        <div class="container carusel-4">
                            <div class="col-lg-2 col-md-2 col-sm-1 hidden-xs"></div>
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 carusel-4-col">
                                <img class="img-responsive center-block"
                                     src="<?php bloginfo("template_directory"); ?>/img/singapur-img.png" alt="">
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 carusel-4-col">
                                <img class="img-responsive center-block"
                                     src="<?php bloginfo("template_directory"); ?>/img/quates.png" alt="">
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 carusel-4-col">
                                <p>Бали+Сингапур, ноябрь 2017 г.<br/>
                                    Хотим поделиться своими впечатлениями от нашего отпуска и сотрудничества с
                                    агентством ЦТБ. Во-первых, наш турагент Люба оказалась настолько компетентной,
                                    приятной и уверенной в своих действиях, что мы просто мгновенно приняли решение
                                    работать только с ней и больше не идти никуда. Во-вторых, само турагенство
                                    порадовало огромным выбором возможностей для самых дальних направлений.<a
                                            href="<?php echo get_permalink(100); ?>">...</a></p>
                                <div class="author">Анастасия Крупина</div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Кнопка-->
            <div class="row lvl-5-parners-padding">
                <form action="<?php echo get_permalink(100); ?>">
                    <button type="submit" class="btn btn-lg red center-block">Оставить отзыв</button>
                </form>
            </div>
        </div>

        <?php wp_footer(); ?>
    </body>
<?php
get_footer();