<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ctb
 */
?>
<footer>
    <div class="container">
        <!--Верхняя часть с картой и левым блоком-->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <!--Блок с контактами-->
                <div class="row">
                    <h2 class="gold contacts-h1"><strong>КОНТАКТЫ</strong></h2>
                </div>
                <div class="row footer-padding">
                    <h4 class="gold"><strong>АДРЕС</strong></h4>
                    <p>г. Ярославль, ул. Победы, д. 43/61</p>
					<p>ТЦ "МИГ" (у Макдональдса), 2-ой этаж</p>
                </div>
                <div class="row footer-padding">
                    <h4 class="gold"><strong>E-MAIL</strong></h4>
                    <p>info@ctb76.ru</p>
                </div>
                <div class="row footer-padding">
                    <h4 class="gold"><strong>ТЕЛЕФОН / WHATSAPP / VIBER</strong></h4>
                    <p>+7 (4852) 94-14-50</p>
					<p>+7 909 280 90 55</p>
                </div>
                <div class="row footer-padding footer-cocial-row">
                    <h4 class="gold"><strong>СОЦИАЛЬНЫЕ СЕТИ</strong></h4>
                    <a class="footer_social" role="button" href="https://vk.com/ctb76" target="_blank" title="Мы Вконтакте">
                        <img class=""
                             src="<?php bloginfo("template_directory"); ?>/img/svg/vk.svg"
                             alt="Мы Вконтакте">
                    </a>
                    <a class="footer_social" href="https://www.instagram.com/ctb76.ru/" target="_blank" title="Мы в Instagram">
                        <img class=""
                             src="<?php bloginfo("template_directory"); ?>/img/svg/instagram.svg"
                             alt="Мы в Instagram">
                    </a>
                    <a class="footer_social" href="https://www.facebook.com/groups/584779218582089/" target="_blank" title="Мы в Facebook">
                        <img class=""
                             src="<?php bloginfo("template_directory"); ?>/img/svg/facebook.svg"
                             alt="Мы в Facebook">
                    </a>
                    <a class="footer_social" href="https://ok.ru/group/55405382270984" target="_blank" title="Мы в Одноклассники">
                        <img class=""
                             src="<?php bloginfo("template_directory"); ?>/img/svg/odnoklassniki-logo (1).svg"
                             alt="Мы в Одноклассники">
                    </a>
                </div>
            </div>
            <!--Блок с картой-->
            <div class="col-md-6 col-lg-6 hidden-xs hidden-sm">
					<iframe src="//yandex.ru/map-widget/v1/?um=constructor%3Afd718fa5b02b5fb71a15f22fec2592ab39146bc79d0aec7366bb31d0f77da6c5&amp;source=constructor" width="100%" height="450" frameborder="0"></iframe>
            </div>
        </div>
        <!--Нижняя часть с логотимом и надписями-->
        <div class="row padd-bot">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4 big-img">
					<h2 class="big-text">
						БОЛЬШЕ ЧЕМ ТУРАГЕНСТВО
					</h2>
                    <!--<img class="img-responsive" src="<?php bloginfo("template_directory");?>/img/big.png" alt="">-->
                </div>
                <div class="">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 ">
                         <ul>
                           <li><a href="<?php echo get_permalink(118); ?>">Фото и Видео</a></li>
                           <li><a href="<?php echo get_permalink(289); ?>">Поиск тура</a></li>
                           <li><a href="<?php echo get_permalink(92); ?>">Горящие</a></li>
                           <li><a href="<?php echo get_permalink(95); ?>">Раннее Бронирование</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <ul>
                            <li><a href="<?php echo get_permalink(97); ?>">Акции и Бонусы</a></li>
                            <li><a href="<?php echo get_permalink(100); ?>">Отзывы</a></li>
                            <li><a href="<?php echo get_permalink(103); ?>">Услуги</a></li>
                            <li><a href="<?php echo get_permalink(105); ?>">Программа привилегий</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--Самый низ-->
    </div>
</footer>
</html>