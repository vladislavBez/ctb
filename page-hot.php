<?php
/*
Template Name: Страница Горячие Спецпредложения
*/
include 'header-page.php';
?>
<!-- Modal -->
<div class="modal fade" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Подобрать тур</h4></center>
                </div>
                <div class="modal-body">
                    <form action="https://ctb76.ru/podbor.php" class="contact" method="post" name="test" id="form">
                        <label for="name">ФИО:</label>
                        <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name" class="form-control" required="required"></p>
                        <label for="phone">Телефон:</label>
                        <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone" class="form-control" required="required"></p>
                        <label for="email">E-mail:</label>
                        <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail" class="form-control" required="required"></p>
                        <label for="message">Сообщение:</label><br/>
                        <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
						<label for="name" style="color: black;">Введите цифры с картинки:</label>
                        <!-- вывод капчи из файла captcha.php -->
						<p><?php require ("captcha.php"); ?></p>
                        <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                        <center><input type="submit" class="btn btn-lg red"  id="submit" name="form" value="Отправить"></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
<!--Modal-->
    <!--Modal-->
    <div class="modal fade" id="myModal-3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Подбор горячих туров</h4></center>
                </div>
                <div class="modal-body">
                    <?php echo do_shortcode('[contact-form-7 id="473" title="Горячие туры"]');?>
                </div>
            </div>
        </div>
    </div>
    <!--Modal-->
<body>
<div class="container-fluid banner-hot">
    <div class="row">
        <div class="container">
            <h1 class="white event-h1">ГОРЯЧИЕ <br>СПЕЦПРЕДЛОЖЕНИЯ</h1>
        </div>
    </div>
</div>
	<div class="container padd-top">
<script type="text/javascript" src="https://ui.sletat.ru/module-4.0/core.js" charset="utf-8"></script>
<script type="text/javascript">sletat.FrameHot.$create({
  toursCount        : 8,
  useCard           : false,
  agencyContact2    : {
    header          : "ЦТБ - Центр Туристического Бронирования",
    phone           : "+7(4852) 94-14-50",
    email           : "info@ctb76.ru"
  },
  enabledCurrencies : ["RUB"]
});</script>
<span class="sletat-copyright">Идет загрузка модуля <a href="https://sletat.ru/" title="поиск туров" target="_blank">поиска туров</a> &hellip;</span>
	</div>
<div class="container-fluid back-black">
    <div class="row text-center partners-logo">
        <h2 class="gold text-size-6 second-title-hot padd-top">ВЫБЕРИ СВОЙ ВАРИАНТ</h2>
    </div>
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd-10 center-block">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 hot-chs-1 ">

                    <div class="choice-div">
                    <strong class="text-size-4 white choice-strong">01</strong>
                    </div>
                    <div class="choice-div">
                        <p class="text-size-4 white">Хочу получить
                            подборку горящих
                            туров
                        </p>
                   </div>
                    <div class="choice-div">
                        <button type="button" class="btn btn-lg red" data-toggle="modal" data-target="#myModal-3">ПОЛУЧИТЬ ПОДБОРКУ</button>
                    </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 hot-chs-2"><div class="choice-div">

                <strong class="text-size-4 white choice-strong">02</strong>
            </div>
                <div class="choice-div">
                    <p class="text-size-4 white">Посмотреть последние спецпредложения
                    </p>
                </div>
                <div class="choice-div">
					<a href="https://vk.cc/7ZvQvp" target="_blank" class="btn btn-lg red">ПОСМОТРЕТЬ</a>					
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid hot-padd">
    <div class="container">
        <strong class="text-size-4 hot-text">ЧТО ТАКОЕ ГОРЯЩИЕ?</strong>
        <p>Это туры, на которые ТО начинают снижать цены ближе к датам поездки, т. к. эти путёвки не продаются. ТО лучше
            продать совсем дешёво, чем не продать совсем. Купил Туроператор самолет, оптом номера в разных отелях,
            страховки, организовал групповой трансфет, сложил всё это воедино в турпакеты и продаёт. Средства вложены
            Туроператором заранее, понятно, что продавать надо, во что бы то ни стало. В какой-то момент становиться
            понятно, что на определённые даты самолёт не заполняется и определённые номера в отелях не куплены. Вот на
            них и снижаются цены. Так было раньше!! Сейчас царит так называемое динамическое пакетирование, когда
            турпакеты формируются в режиме реального времени по действующим тарифам поставщиков (авиакомпаний, отелей и
            прочих). У ТО отсутствую риски не продать, ранее ими выкупленное. Гореть просто НЕЧЕМУ. Горящие изживают
            себя как вид!</p>
    </div>
</div>
<div class="container-fluid mulmak-bone hot-padd">
    <div class="container">
        <strong class="text-size-4 hot-text">МИФЫ ПРО ГОРЯЩИЕ:</strong>
        <p>Многие думают, что это отказные туры, уже купленные кем-то, кто не может поехать. Это миф, это не так!</p>
    </div>
</div>
<div class="container-fluid hot-padd">
    <div class="container">
        <strong class="text-size-4 hot-text">ПРЕИМУЩЕСТВА</strong>
        <p>Можно ожидать реально снижения цены.</p>
    </div>
</div>
<div class="container-fluid mulmak-bone hot-padd">
    <div class="container">
        <strong class="text-size-4 hot-text">НЮАНСЫ</strong>
		<p>Горящие, как правило, не горят на Ваши даты, Ваше количество дней, состав, звёздность. Это фортуна, когда и что не будет продаваться, не угадать. Вы подстраиваетесь под них, а не они горят четко по Вашим запросам! Вероятность, что близко к Вашим датам не будет туров или вылетов или подходящих отелей гораздо больше, чем вероятность наличия горящих цен. Горящие - это те, которые НИКТО не купил. Это остатки, самые некачественные, как правило. Не удобные вылеты, не самые лучшие отели, жесткое количество дней. Гореть может тур за 300000 руб, сгореть до 150000. Отличная скидка в 50%! Если Ваш бюджет совсем мал, то и Ваши шансы очень малы. Нужно иметь хотя бы пару недель для вылета и быть готовым собраться за несколько часов.</p>      
    </div>
</div>
<div class="container-fluid hot-padd">
    <div class="container">
        <strong class="text-size-4 hot-text">АЛЬТЕРНАТИВЫ</strong>
        <p>Раннее бронирование - это идеальный вариант не менее выгодно, чем по горящим, уехать, четко по своим
            пожеланиям, иметь большой выбор по отелям, перелётам и т. п.)</p>
    </div>
</div>
	<?php wp_footer(); ?>
<?php get_footer('page'); ?>