<?php
/*
Template Name: Страница Фото и Видео
*/
include 'header-page.php';
?>
<!-- Modal -->
<div class="modal fade" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Подобрать тур</h4></center>
                </div>
                <div class="modal-body">
                    <form action="https://ctb76.ru/podbor.php" class="contact" method="post" name="test" id="form">
                        <label for="name">ФИО:</label>
                        <p><input type="text" name="name" value="" placeholder="Введите ваше ФИО" id="name" class="form-control" required="required"></p>
                        <label for="phone">Телефон:</label>
                        <p><input type="phone" name="phone" value="" placeholder="Введите ваш телефон" id="phone" class="form-control" required="required"></p>
                        <label for="email">E-mail:</label>
                        <p><input type="email" name="email" value="" placeholder="E-mail" id="еmail" class="form-control" required="required"></p>
                        <label for="message">Сообщение:</label><br/>
                        <textarea class="form-control" name="message" cols="40" rows="6"></textarea><br/>
						<label for="name" style="color: black;">Введите цифры с картинки:</label>
                        <!-- вывод капчи из файла captcha.php -->
						<p><?php require ("captcha.php"); ?></p>
                        <input name="captcha_validation" type="text" size="6" maxlength="5" required="required"><br/>
                        <center><input type="submit" class="btn btn-lg red"  id="submit" name="form" value="Отправить"></center>
                    </form>
                </div>
            </div>
        </div>
    </div>
<!--Modal-->
<body>
<div class="container-fluid photo-banner-back">
        <div class="container">
            <h1 class="white event-h1">ФОТО <br>И ВИДЕО</h1>
        </div>
</div>
<div class="container-fluid back-black">
    <div class="container photo-container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!--первые три-->
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img class="img-responsive" src="<?php bloginfo("template_directory");?>/img/AAEAAQAAAAAAAAnZAAAAJGE0OTgzNjdlLWNlMzAtNDkzMC1hNWI1LTk5MDRlNTc2ZTU5Zg.png" alt="">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 photo-th">
                    <strong class="white photo-text">НАШИ ПОЕЗДКИ</strong>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img class="img-responsive" src="<?php bloginfo("template_directory");?>/img/photo-2.png" alt="">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 photo-th">
                    <strong class="white photo-text">ФОТО ТУРИСТОВ</strong>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img src="<?php bloginfo("template_directory");?>/img/photo-3.png" class="img-responsive" alt="">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 photo-th photo-top-container">
                    <p class="white photo-text photo-top ">ТОПы: отели, страны, города,<br>
                        курорты, пляжи, достопримечательности</p>
                </div>
            </div>
            <!--вторые три-->
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img src="<?php bloginfo("template_directory");?>/img/photo-4.png" class="img-responsive" alt="">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 photo-th">
                    <strong class="white photo-text">ВИДЕО</strong>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="capital_city_block" style="padding-right: 0; padding-left: 0;">
                    <div class="photo-video-slider">
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/0ahZFBy7xCo.jpg"
                               data-fancybox="gallery">
                                <img class="img-responsive"
                                     src="<?php bloginfo("template_directory"); ?>/images/0ahZFBy7xCo.png"/>
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/6ZZO5U1ULEc.jpg"
                               data-fancybox="gallery">
                                <img class="img-responsive"
                                     src="<?php bloginfo("template_directory"); ?>/images/6ZZO5U1ULEc.png"/>
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/D1T4tK50AGI.jpg"
                               data-fancybox="gallery">
                                <img class="img-responsive"
                                     src="<?php bloginfo("template_directory"); ?>/images/D1T4tK50AGI.png"/>
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/ffiC9YLdXRM.jpg"
                               data-fancybox="gallery">
                                <img class="img-responsive"
                                     src="<?php bloginfo("template_directory"); ?>/images/ffiC9YLdXRM.png"/>
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/FyKp0hn0its.jpg"
                               data-fancybox="gallery">
                                <img class="img-responsive"
                                     src="<?php bloginfo("template_directory"); ?>/images/FyKp0hn0its.png"/>
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/JN4eweys4ZM.jpg"
                               data-fancybox="gallery">
                                <img class="img-responsive"
                                     src="<?php bloginfo("template_directory"); ?>/images/JN4eweys4ZM.png"/>
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/QAMO5NoakIk.jpg"
                               data-fancybox="gallery">
                                <img class="img-responsive"
                                     src="<?php bloginfo("template_directory"); ?>/images/QAMO5NoakIk.png"/>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 photo-th">
                    <a href="#gallery-1" >
                        <strong class="white photo-text" id="capital_city">СТОЛИЦЫ МИРА</strong>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <img src="<?php bloginfo("template_directory");?>/img/photo-6.png" class="img-responsive" alt="...">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 photo-th">
                    <strong class="white photo-text">УЛЫБНИСЬ</strong>
                </div>
                </div>
            </div>
        </div>
    </div>
	<?php wp_footer(); ?>
<?php get_footer('page'); ?>